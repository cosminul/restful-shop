package ro.hume.cosmin.restful.shop.api.data;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class ProductDataTest {

	@Test
	public void equalsContract() {
		EqualsVerifier.forClass(ProductData.class).suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
				.verify();
	}
}

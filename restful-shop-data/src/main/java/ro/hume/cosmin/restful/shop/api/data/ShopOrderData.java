package ro.hume.cosmin.restful.shop.api.data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Representation of an order.
 */
public class ShopOrderData {

	private String uuid;

	private String email;

	private LocalDateTime orderTime;

	private List<FixedPriceProductData> products;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDateTime getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(LocalDateTime orderTime) {
		this.orderTime = orderTime;
	}

	public List<FixedPriceProductData> getProducts() {
		return products;
	}

	public void setProducts(List<FixedPriceProductData> products) {
		this.products = products;
	}

	/**
	 * Calculates the order amount as the sum of the prices for all products in the
	 * order.
	 * 
	 * @return the order amount
	 */
	@JsonIgnore
	public BigDecimal getAmount() {
		if (products == null) {
			return BigDecimal.ZERO;
		}
		return products.stream().map(FixedPriceProductData::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((orderTime == null) ? 0 : orderTime.hashCode());
		result = prime * result + ((products == null) ? 0 : products.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ShopOrderData)) {
			return false;
		}
		ShopOrderData other = (ShopOrderData) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (orderTime == null) {
			if (other.orderTime != null) {
				return false;
			}
		} else if (!orderTime.equals(other.orderTime)) {
			return false;
		}
		if (products == null) {
			if (other.products != null) {
				return false;
			}
		} else if (!products.equals(other.products)) {
			return false;
		}
		if (uuid == null) {
			if (other.uuid != null) {
				return false;
			}
		} else if (!uuid.equals(other.uuid)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ShopOrderData [uuid=" + uuid + ", email=" + email + ", orderTime=" + orderTime + ", products="
				+ products + "]";
	}
}

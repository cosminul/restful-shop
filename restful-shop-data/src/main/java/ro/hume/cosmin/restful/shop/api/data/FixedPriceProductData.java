package ro.hume.cosmin.restful.shop.api.data;

import java.math.BigDecimal;

/**
 * A product-price pair belonging to an order.
 */
public class FixedPriceProductData {

	private ProductData product;

	private BigDecimal price;

	public ProductData getProduct() {
		return product;
	}

	public void setProduct(ProductData product) {
		this.product = product;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FixedPriceProductData)) {
			return false;
		}
		FixedPriceProductData other = (FixedPriceProductData) obj;
		if (price == null) {
			if (other.price != null) {
				return false;
			}
		} else if (!price.equals(other.price)) {
			return false;
		}
		if (product == null) {
			if (other.product != null) {
				return false;
			}
		} else if (!product.equals(other.product)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FixedPriceProductData [product=" + product + ", price=" + price + "]";
	}
}

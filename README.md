# RESTful Shop

An example [RESTful](http://en.wikipedia.org/wiki/Representational_state_transfer) web service in Java using [Spring Boot (RestController)](https://spring.io/guides/gs/rest-service/).

## How to run

You need **Java 8** and **Maven** installed.

### Install the data module

    cd restful-shop-data
    mvn clean install

### Install the core module

    cd restful-shop-core
    mvn clean install

### Run the service

    mvn spring-boot:run

### Run the acceptance test

    cd restful-shop-acceptance-tests
    mvn clean verify

## API Docs

After building the project, find the API docs in:

    restful-shop/restful-shop-core/target/doc/apidocs/index.html

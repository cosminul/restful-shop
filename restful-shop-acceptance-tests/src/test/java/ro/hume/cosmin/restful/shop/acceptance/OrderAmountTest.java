package ro.hume.cosmin.restful.shop.acceptance;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import ro.hume.cosmin.restful.shop.api.data.FixedPriceProductData;
import ro.hume.cosmin.restful.shop.api.data.ProductData;
import ro.hume.cosmin.restful.shop.api.data.ShopOrderData;

public class OrderAmountTest {

	private static final DateTimeFormatter FORMAT_DATE_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private static final String PRODUCT_NAME = "Test Product";
	private static final double INITIAL_PRICE = 0.99;
	private static final double REDUCED_PRICE = 0.49;

	@Test
	public void testOrderAmount() {
		ProductData createdProduct = createProduct(PRODUCT_NAME, INITIAL_PRICE);
		ShopOrderData placedOrder = placeOrder(createdProduct);
		updateProduct(createdProduct, REDUCED_PRICE);
		ShopOrderData recordedOrder = findOrder(placedOrder.getUuid());
		assertEquals(placedOrder.getAmount(), recordedOrder.getAmount());
	}

	private ProductData createProduct(String name, double price) {
		//@formatter:off
		return
		given()
			.port(8080)
		.when()
			.contentType("application/json")
			.body(product(name, price))
			.post("/products")
		.then()
			.extract().body().as(ProductData.class);
		//@formatter:off
	}

	private ShopOrderData placeOrder(ProductData productToOrder) {
		//@formatter:off
		return
		given()
			.port(8080)
		.when()
			.contentType("application/json")
			.body(orderForProduct(productToOrder))
			.post("/orders")
		.then()
			.extract().body().as(ShopOrderData.class);
		//@formatter:off
	}

	private void updateProduct(ProductData product, double price) {
		//@formatter:off
		given()
			.port(8080)
			.pathParam("id", product.getId())
		.when()
			.contentType("application/json")
			.body(product(product.getId(), product.getName(), price))
			.patch("/products/{id}")
		.then()
			.statusCode(200);
		//@formatter:off
	}

	private ShopOrderData[] ordersBetween(String dateFrom, String dateTo) {
		//@formatter:off
		return
		given()
			.port(8080)
		.when()
			.contentType("application/json")
			.get("/orders/?from={from}&to={to}", dateFrom, dateTo)
		.then()
			.extract().body().as(ShopOrderData[].class);
		//@formatter:on
	}

	private ProductData product(long id, String name, double price) {
		ProductData product = product(name, price);
		product.setId(id);
		return product;
	}

	private ProductData product(String name, double price) {
		ProductData product = new ProductData();
		product.setName(name);
		product.setPrice(BigDecimal.valueOf(price));
		return product;
	}

	private ShopOrderData orderForProduct(ProductData product) {
		FixedPriceProductData orderedProduct = new FixedPriceProductData();
		orderedProduct.setProduct(product);
		orderedProduct.setPrice(product.getPrice());

		ShopOrderData order = new ShopOrderData();
		order.setEmail("someone@example.com");
		order.setProducts(Collections.singletonList(orderedProduct));

		return order;
	}

	private String yesterday() {
		return LocalDateTime.now().minusDays(1).format(FORMAT_DATE_TIME);
	}

	private String tomorrow() {
		return LocalDateTime.now().plusDays(1).format(FORMAT_DATE_TIME);
	}

	private ShopOrderData findOrder(String uuid) {
		return Arrays.stream(ordersBetween(yesterday(), tomorrow())).filter(order -> uuid.equals(order.getUuid()))
				.findFirst().orElseThrow(RuntimeException::new);
	}
}

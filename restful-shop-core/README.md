# How to run

You need **Java 8** and **Maven** installed.

To run the application on the default port `8080`:

    mvn spring-boot:run

If your local `8080` port is already in use, you can choose another one:

    mvn spring-boot:run -Dserver.port=9090

package ro.hume.cosmin.restful.shop.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.hume.cosmin.restful.shop.dao.ProductRepository;
import ro.hume.cosmin.restful.shop.model.Product;

@Service
public class DefaultProductService implements ProductService {

	@Autowired
	private ProductRepository productDao;

	@Override
	public Product create(Product product) {
		return productDao.save(product);
	}

	@Override
	public List<Product> retrieveAll() {
		List<Product> products = new LinkedList<>();
		for (Product product : productDao.findAll()) {
			products.add(product);
		}
		return products;
	}

	@Override
	public Product update(Product product) {
		return productDao.save(product);
	}
}

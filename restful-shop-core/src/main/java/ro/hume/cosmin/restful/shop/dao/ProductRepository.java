package ro.hume.cosmin.restful.shop.dao;

import org.springframework.data.repository.CrudRepository;

import ro.hume.cosmin.restful.shop.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
}

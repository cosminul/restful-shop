package ro.hume.cosmin.restful.shop.service;

import java.time.LocalDateTime;
import java.util.List;

import ro.hume.cosmin.restful.shop.model.ShopOrder;

public interface ShopOrderService {

	ShopOrder create(ShopOrder shopOrder);

	List<ShopOrder> retrieve(LocalDateTime from, LocalDateTime to);
}

package ro.hume.cosmin.restful.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulShop {

	public static void main(String[] args) {
		SpringApplication.run(RestfulShop.class, args);
	}
}

package ro.hume.cosmin.restful.shop.service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.hume.cosmin.restful.shop.dao.ShopOrderRepository;
import ro.hume.cosmin.restful.shop.model.FixedPriceProduct;
import ro.hume.cosmin.restful.shop.model.ShopOrder;

@Service
public class DefaultShopOrderService implements ShopOrderService {

	@Autowired
	private ShopOrderRepository orderDao;

	@Override
	public ShopOrder create(ShopOrder shopOrder) {
		shopOrder.setUuid(UUID.randomUUID().toString());
		shopOrder.setOrderTime(LocalDateTime.now());
		if (shopOrder.getProducts() != null) {
			for (FixedPriceProduct fixedPriceProduct : shopOrder.getProducts()) {
				fixedPriceProduct.setShopOrder(shopOrder);
			}
		}
		return orderDao.save(shopOrder);
	}

	@Override
	public List<ShopOrder> retrieve(LocalDateTime from, LocalDateTime to) {
		List<ShopOrder> shopOrders = new LinkedList<>();
		for (ShopOrder shopOrder : orderDao.find(from, to)) {
			shopOrders.add(shopOrder);
		}
		return shopOrders;
	}
}

package ro.hume.cosmin.restful.shop.api;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.websocket.server.PathParam;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ro.hume.cosmin.restful.shop.api.data.ProductData;
import ro.hume.cosmin.restful.shop.model.Product;
import ro.hume.cosmin.restful.shop.service.ProductService;

/**
 * Operations on products.
 */
@RestController
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * Creates a new product.
	 * 
	 * @param productData the data defining the product to create
	 * @return the product that was created
	 */
	@PostMapping("/products")
	@ResponseStatus(HttpStatus.CREATED)
	public ProductData create(@RequestBody ProductData productData) {
		Product productModel = modelMapper.map(productData, Product.class);
		Product createdProductModel = productService.create(productModel);
		return modelMapper.map(createdProductModel, ProductData.class);
	}

	/**
	 * Retrieves all products.
	 * 
	 * @return a list of all products
	 */
	@GetMapping("/products")
	public List<ProductData> retrieve() {
		return productService.retrieveAll().stream().map(product -> modelMapper.map(product, ProductData.class))
				.collect(toList());
	}

	/**
	 * Updates a product.
	 * 
	 * @param id          the ID of the product to update
	 * @param productData the updated description of the product
	 */
	@PatchMapping("/products/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathParam("id") Long id, @RequestBody ProductData productData) {
		Product productModel = modelMapper.map(productData, Product.class);
		productService.update(productModel);
	}
}

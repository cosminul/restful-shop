package ro.hume.cosmin.restful.shop.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ro.hume.cosmin.restful.shop.model.ShopOrder;

public interface ShopOrderRepository extends CrudRepository<ShopOrder, Long> {

	/**
	 * Finds all orders placed between two specified dates.
	 * 
	 * @param from the initial date
	 * @param to   the final date
	 * @return A list of orders whose date is after {@code from} and before
	 *         {@code to}. If no orders are found, this method returns an empty
	 *         list.
	 */
	@Query("SELECT o FROM ShopOrder o WHERE o.orderTime >= :from AND o.orderTime <= :to")
	public List<ShopOrder> find(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to);
}

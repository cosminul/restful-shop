package ro.hume.cosmin.restful.shop.service;

import java.util.List;

import ro.hume.cosmin.restful.shop.model.Product;

public interface ProductService {

	Product create(Product product);

	List<Product> retrieveAll();

	Product update(Product product);
}

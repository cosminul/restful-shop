package ro.hume.cosmin.restful.shop.api;

import static java.util.stream.Collectors.toList;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ro.hume.cosmin.restful.shop.api.data.ShopOrderData;
import ro.hume.cosmin.restful.shop.model.ShopOrder;
import ro.hume.cosmin.restful.shop.service.ShopOrderService;

/**
 * Operations on orders.
 */
@RestController
public class ShopOrderController {

	private static final String DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	private static final DateTimeFormatter FORMAT_DATE_TIME = DateTimeFormatter.ofPattern(DATE_TIME);

	@Autowired
	private ShopOrderService shopOrderService;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * Creates a new order.
	 * 
	 * @param shopOrderData the data defining the order to be created
	 * @return the order that was created
	 */
	@PostMapping("/orders")
	@ResponseStatus(HttpStatus.CREATED)
	public ShopOrderData create(@RequestBody ShopOrderData shopOrderData) {
		ShopOrder shopOrderModel = modelMapper.map(shopOrderData, ShopOrder.class);
		ShopOrder createdOrder = shopOrderService.create(shopOrderModel);
		return modelMapper.map(createdOrder, ShopOrderData.class);
	}

	/**
	 * Retrieves all orders in a time interval.
	 * 
	 * @param from the minimum date of an order to be considered
	 * @param to   the maximum date of an order to be considered
	 * @return a list of orders placed in the specified interval
	 */
	@GetMapping("/orders")
	public List<ShopOrderData> retrieve(@RequestParam("from") @DateTimeFormat(pattern = DATE_TIME) String from,
			@RequestParam("to") @DateTimeFormat(pattern = DATE_TIME) String to) {
		LocalDateTime dateFrom = LocalDateTime.parse(from, FORMAT_DATE_TIME);
		LocalDateTime dateTo = LocalDateTime.parse(to, FORMAT_DATE_TIME);
		return shopOrderService.retrieve(dateFrom, dateTo).stream()
				.map(shopOrder -> modelMapper.map(shopOrder, ShopOrderData.class)).collect(toList());
	}
}

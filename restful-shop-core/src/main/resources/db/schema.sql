CREATE TABLE product(
	id INT PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	price DECIMAL NOT NULL
);

CREATE TABLE shop_order(
	id INT PRIMARY KEY,
	uuid VARCHAR(64) NOT NULL,
	email VARCHAR(255) NOT NULL,
	order_time TIMESTAMP NOT NULL
);

CREATE TABLE shop_order_product(
	id INT PRIMARY KEY,
	shop_order_id INT, /* Do not require not-null because Hibernate may want to save this before having created the shop_order. */
	product_id INT NOT NULL,
	price DECIMAL NOT NULL,
	-- FOREIGN KEY (shop_order_id) REFERENCES shop_order(id), /* Do not require this because Hibernate may want to save this before having created the shop_order.  */
	FOREIGN KEY (product_id) REFERENCES product(id),
);

package ro.hume.cosmin.restful.shop.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import ro.hume.cosmin.restful.shop.dao.ShopOrderRepository;
import ro.hume.cosmin.restful.shop.model.ShopOrder;

public class DefaultShopOrderServiceTest {

	@Mock
	private ShopOrderRepository shopOrderDao;

	@InjectMocks
	private DefaultShopOrderService shopOrderService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreate() {
		ShopOrder input = new ShopOrder();
		when(shopOrderDao.save(input)).thenReturn(input);

		shopOrderService.create(input);

		verify(shopOrderDao).save(input);
		verifyNoMoreInteractions(shopOrderDao);
	}
}

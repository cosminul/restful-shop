package ro.hume.cosmin.restful.shop.api;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ro.hume.cosmin.restful.shop.test.JsonMapper.asJsonString;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import ro.hume.cosmin.restful.shop.api.data.FixedPriceProductData;
import ro.hume.cosmin.restful.shop.api.data.ProductData;
import ro.hume.cosmin.restful.shop.api.data.ShopOrderData;
import ro.hume.cosmin.restful.shop.model.FixedPriceProduct;
import ro.hume.cosmin.restful.shop.model.Product;
import ro.hume.cosmin.restful.shop.model.ShopOrder;
import ro.hume.cosmin.restful.shop.service.ShopOrderService;

@RunWith(SpringRunner.class)
@WebMvcTest(ShopOrderController.class)
public class ShopOrderControllerTest {

	private static final long PRODUCT_ID = 1L;

	private static final String PRODUCT_NAME = "test";

	private static final BigDecimal PRODUCT_PRICE = BigDecimal.valueOf(0.99);

	private static final String CUSTOMER_EMAIL = "someone@example.com";

	private static final long ORDER_ID = 1L;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ShopOrderService shopOrderService;

	@MockBean
	private ModelMapper modelMapper;

	@Test
	public void testCreate() throws Exception {
		ShopOrderData inputData = inputOrderData();
		ShopOrder inputModel = inputOrderModel();
		ShopOrderData outputData = outputOrderData();
		ShopOrder outputModel = outputOrderModel();

		when(modelMapper.map(inputData, ShopOrder.class)).thenReturn(inputModel);
		when(shopOrderService.create(inputModel)).thenReturn(outputModel);
		when(modelMapper.map(outputModel, ShopOrderData.class)).thenReturn(outputData);

		mockMvc.perform(post("/orders").content(asJsonString(inputData)).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isCreated())
				.andExpect(content().json(asJsonString(outputData)));

		verify(modelMapper).map(inputData, ShopOrder.class);
		verify(shopOrderService).create(inputModel);
		verify(modelMapper).map(outputModel, ShopOrderData.class);
		verifyNoMoreInteractions(modelMapper, shopOrderService);
	}

	private ShopOrderData inputOrderData() {
		ProductData productData = new ProductData();
		productData.setId(PRODUCT_ID);
		productData.setName(PRODUCT_NAME);

		FixedPriceProductData fixedPriceProductData = new FixedPriceProductData();
		fixedPriceProductData.setProduct(productData);
		fixedPriceProductData.setPrice(PRODUCT_PRICE);
		List<FixedPriceProductData> productsData = Arrays.asList(fixedPriceProductData);

		ShopOrderData inputData = new ShopOrderData();
		inputData.setEmail(CUSTOMER_EMAIL);
		inputData.setProducts(productsData);

		return inputData;
	}

	private ShopOrder inputOrderModel() {
		Product productModel = new Product();
		productModel.setId(PRODUCT_ID);
		productModel.setName(PRODUCT_NAME);

		FixedPriceProduct fixedPriceProductModel = new FixedPriceProduct();
		fixedPriceProductModel.setProduct(productModel);
		fixedPriceProductModel.setPrice(PRODUCT_PRICE);
		List<FixedPriceProduct> productsModel = Arrays.asList(fixedPriceProductModel);

		ShopOrder inputModel = new ShopOrder();
		inputModel.setEmail(CUSTOMER_EMAIL);
		inputModel.setProducts(productsModel);

		return inputModel;
	}

	private ShopOrderData outputOrderData() {
		ShopOrderData outputData = inputOrderData();
		return outputData;
	}

	private ShopOrder outputOrderModel() {
		ShopOrder outputModel = inputOrderModel();
		outputModel.setId(ORDER_ID);
		return outputModel;
	}
}

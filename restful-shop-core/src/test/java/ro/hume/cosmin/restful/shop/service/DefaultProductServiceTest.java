package ro.hume.cosmin.restful.shop.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import ro.hume.cosmin.restful.shop.dao.ProductRepository;
import ro.hume.cosmin.restful.shop.model.Product;

public class DefaultProductServiceTest {

	private static final String PRODUCT_NAME = "test product";

	@Mock
	private ProductRepository productDao;

	@InjectMocks
	private DefaultProductService productService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreate() {
		Product input = new Product();
		when(productDao.save(input)).thenReturn(input);

		productService.create(input);

		verify(productDao).save(input);
		verifyNoMoreInteractions(productDao);
	}

	@Test
	public void testRetrieveAll() {
		Product product = new Product();
		product.setName(PRODUCT_NAME);
		when(productDao.findAll()).thenReturn(Arrays.asList(product));

		List<Product> products = productService.retrieveAll();

		assertEquals(1, products.size());
		assertEquals(product, products.get(0));
	}

	@Test
	public void testUpdate() {
		Product input = new Product();
		input.setName(PRODUCT_NAME);
		when(productDao.save(input)).thenReturn(input);

		productService.update(input);

		verify(productDao).save(input);
		verifyNoMoreInteractions(productDao);
	}
}

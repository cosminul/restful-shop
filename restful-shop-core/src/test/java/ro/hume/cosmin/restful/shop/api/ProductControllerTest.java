package ro.hume.cosmin.restful.shop.api;

import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ro.hume.cosmin.restful.shop.test.JsonMapper.asJsonString;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import ro.hume.cosmin.restful.shop.api.data.ProductData;
import ro.hume.cosmin.restful.shop.model.Product;
import ro.hume.cosmin.restful.shop.service.ProductService;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

	private static final long PRODUCT_ID = 1L;

	private static final String PRODUCT_NAME = "test";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductService productService;

	@MockBean
	private ModelMapper modelMapper;

	@Test
	public void testCreate() throws Exception {
		ProductData inputData = new ProductData();
		inputData.setName(PRODUCT_NAME);

		Product inputModel = new Product();
		inputModel.setName(PRODUCT_NAME);

		ProductData outputData = new ProductData();
		outputData.setId(PRODUCT_ID);
		outputData.setName(PRODUCT_NAME);

		Product outputModel = new Product();
		inputModel.setId(PRODUCT_ID);
		inputModel.setName(PRODUCT_NAME);

		when(modelMapper.map(inputData, Product.class)).thenReturn(inputModel);
		when(productService.create(inputModel)).thenReturn(outputModel);
		when(modelMapper.map(outputModel, ProductData.class)).thenReturn(outputData);

		mockMvc.perform(post("/products").content(asJsonString(inputData)).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isCreated())
				.andExpect(content().json(asJsonString(outputData)));

		verify(modelMapper).map(inputData, Product.class);
		verify(productService).create(inputModel);
		verify(modelMapper).map(outputModel, ProductData.class);
		verifyNoMoreInteractions(modelMapper, productService);
	}

	@Test
	public void testRetrieve() throws Exception {
		ProductData productData = new ProductData();
		productData.setId(PRODUCT_ID);
		productData.setName(PRODUCT_NAME);
		List<ProductData> products = Arrays.asList(productData);

		Product productModel = new Product();
		productModel.setId(PRODUCT_ID);
		productModel.setName(PRODUCT_NAME);

		when(productService.retrieveAll()).thenReturn(Arrays.asList(productModel));
		when(modelMapper.map(productModel, ProductData.class)).thenReturn(productData);

		mockMvc.perform(get("/products").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().json(asJsonString(products)));

		verify(productService).retrieveAll();
		verify(modelMapper).map(productModel, ProductData.class);
		verifyNoMoreInteractions(productService, modelMapper);
	}

	@Test
	public void testUpdate() throws Exception {
		ProductData inputData = new ProductData();
		inputData.setId(PRODUCT_ID);
		inputData.setName(PRODUCT_NAME);
		Product inputModel = new Product();
		inputModel.setName(PRODUCT_NAME);

		when(modelMapper.map(inputData, Product.class)).thenReturn(inputModel);

		mockMvc.perform(patch("/products/1").content(asJsonString(inputData)).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(isEmptyString()));

		verify(modelMapper).map(inputData, Product.class);
		verify(productService).update(inputModel);
		verifyNoMoreInteractions(modelMapper, productService);
	}
}

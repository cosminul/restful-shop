package ro.hume.cosmin.restful.shop.api.data;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;

import org.junit.Test;
import org.modelmapper.ModelMapper;

import ro.hume.cosmin.restful.shop.model.FixedPriceProduct;
import ro.hume.cosmin.restful.shop.model.Product;
import ro.hume.cosmin.restful.shop.model.ShopOrder;

public class ShopOrderDataTest {

	private static final long PRODUCT_1_ID = 1L;
	private static final String PRODUCT_1_NAME = "bread";
	private static final BigDecimal PRODUCT_1_PRICE = BigDecimal.valueOf(0.99);
	private static final long PRODUCT_2_ID = 2L;
	private static final String PRODUCT_2_NAME = "cheese";
	private static final BigDecimal PRODUCT_2_PRICE = BigDecimal.valueOf(1.49);
	private static final long ORDER_ID = 1L;
	private static final String CUSTOMER_EMAIL = "someone@example.com";
	private static final String UUID = "12345678-abcd-0000-9999-0123456789ab";
	private static final LocalDateTime ORDER_TIME = LocalDateTime.of(2018, 12, 10, 12, 34, 56);

	private ModelMapper modelMapper = new ModelMapper();

	@Test
	public void testConvertModelToData() {
		FixedPriceProduct fixedPriceBread = fixedPriceBread();
		FixedPriceProduct fixedPriceCheese = fixedPriceCheese();

		ShopOrder model = new ShopOrder();
		model.setId(ORDER_ID);
		model.setEmail(CUSTOMER_EMAIL);
		model.setUuid(UUID);
		model.setOrderTime(ORDER_TIME);
		model.setProducts(Arrays.asList(fixedPriceBread, fixedPriceCheese));
		fixedPriceBread.setShopOrder(model);

		ShopOrderData data = modelMapper.map(model, ShopOrderData.class);

		assertEquals(CUSTOMER_EMAIL, data.getEmail());
		assertEquals(UUID, data.getUuid());
		assertEquals(ORDER_TIME, data.getOrderTime());
		assertEquals(2, data.getProducts().size());
		assertEquals(PRODUCT_1_PRICE.add(PRODUCT_2_PRICE), data.getAmount());
	}

	private FixedPriceProduct fixedPriceBread() {
		Product bread = new Product();
		bread.setId(PRODUCT_1_ID);
		bread.setName(PRODUCT_1_NAME);
		bread.setPrice(PRODUCT_1_PRICE);

		FixedPriceProduct fixedPriceBread = new FixedPriceProduct();
		fixedPriceBread.setId(PRODUCT_1_ID);
		fixedPriceBread.setProduct(bread);
		fixedPriceBread.setPrice(PRODUCT_1_PRICE);

		return fixedPriceBread;
	}

	private FixedPriceProduct fixedPriceCheese() {
		Product cheese = new Product();
		cheese.setId(PRODUCT_2_ID);
		cheese.setName(PRODUCT_2_NAME);
		cheese.setPrice(PRODUCT_2_PRICE);

		FixedPriceProduct fixedPriceCheese = new FixedPriceProduct();
		fixedPriceCheese.setId(PRODUCT_2_ID);
		fixedPriceCheese.setProduct(cheese);
		fixedPriceCheese.setPrice(PRODUCT_2_PRICE);

		return fixedPriceCheese;
	}

	@Test
	public void testConvertDataToModel() {
		ProductData product = new ProductData();
		product.setId(PRODUCT_1_ID);
		product.setName(PRODUCT_1_NAME);
		product.setPrice(PRODUCT_1_PRICE);

		FixedPriceProductData fixedPriceProduct = new FixedPriceProductData();
		fixedPriceProduct.setProduct(product);
		fixedPriceProduct.setPrice(PRODUCT_1_PRICE);

		ShopOrderData data = new ShopOrderData();
		data.setEmail(CUSTOMER_EMAIL);
		data.setUuid(UUID);
		data.setOrderTime(ORDER_TIME);
		data.setProducts(Arrays.asList(fixedPriceProduct));

		ShopOrder model = modelMapper.map(data, ShopOrder.class);

		assertEquals(CUSTOMER_EMAIL, model.getEmail());
		assertEquals(UUID, model.getUuid());
		assertEquals(ORDER_TIME, model.getOrderTime());
		assertEquals(1, model.getProducts().size());
	}
}

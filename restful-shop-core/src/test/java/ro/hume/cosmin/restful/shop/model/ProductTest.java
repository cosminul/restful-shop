package ro.hume.cosmin.restful.shop.model;

import org.junit.Test;

import com.jparams.verifier.tostring.NameStyle;
import com.jparams.verifier.tostring.ToStringVerifier;

import nl.jqno.equalsverifier.EqualsVerifier;

public class ProductTest {

	@Test
	public void equalsContract() {
		EqualsVerifier.forClass(Product.class).verify();
	}

	@Test
	public void testToString() {
		ToStringVerifier.forClass(Product.class).withClassName(NameStyle.SIMPLE_NAME).verify();
	}
}

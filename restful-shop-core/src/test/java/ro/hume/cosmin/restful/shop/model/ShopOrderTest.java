package ro.hume.cosmin.restful.shop.model;

import org.junit.Test;

import com.jparams.verifier.tostring.NameStyle;
import com.jparams.verifier.tostring.ToStringVerifier;

import nl.jqno.equalsverifier.EqualsVerifier;

public class ShopOrderTest {

	@Test
	public void equalsContract() {
		EqualsVerifier.forClass(ShopOrder.class)
				.withPrefabValues(FixedPriceProduct.class, fixedPriceProductWithId(1), fixedPriceProductWithId(2))
				.verify();
	}

	private FixedPriceProduct fixedPriceProductWithId(long id) {
		FixedPriceProduct fixedPrice = new FixedPriceProduct();
		fixedPrice.setId(id);
		return fixedPrice;
	}

	@Test
	public void testToString() {
		ToStringVerifier.forClass(ShopOrder.class).withClassName(NameStyle.SIMPLE_NAME).verify();
	}
}

package ro.hume.cosmin.restful.shop.api.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import ro.hume.cosmin.restful.shop.dao.ProductRepository;
import ro.hume.cosmin.restful.shop.model.Product;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryIT {

	private static final String PRODUCT_NAME = "test";
	private static final BigDecimal PRODUCT_PRICE = BigDecimal.valueOf(0.99);
	private static final BigDecimal PRODUCT_PRICE_REDUCED = BigDecimal.valueOf(0.89);

	@Autowired
	private ProductRepository productDao;

	@Test
	public void testSaveCreate() {
		Product product = new Product();
		product.setName(PRODUCT_NAME);
		product.setPrice(PRODUCT_PRICE);

		Product savedProduct = productDao.save(product);

		assertNotNull(savedProduct.getId());
		assertNotEquals(Long.valueOf(0), savedProduct.getId());
		assertEquals(PRODUCT_NAME, savedProduct.getName());
		assertEquals(PRODUCT_PRICE, savedProduct.getPrice());
	}

	@Test
	public void testSaveUpdate() {
		Product product = new Product();
		product.setName(PRODUCT_NAME);
		product.setPrice(PRODUCT_PRICE);

		Product createdProduct = productDao.save(product);

		createdProduct.setPrice(PRODUCT_PRICE_REDUCED);
		Product updatedProduct = productDao.save(product);

		assertEquals(createdProduct.getId(), updatedProduct.getId());
		assertEquals(createdProduct.getName(), updatedProduct.getName());
		assertEquals(PRODUCT_PRICE_REDUCED, updatedProduct.getPrice());
	}
}

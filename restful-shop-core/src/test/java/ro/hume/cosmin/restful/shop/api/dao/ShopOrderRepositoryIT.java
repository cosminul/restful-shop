package ro.hume.cosmin.restful.shop.api.dao;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import ro.hume.cosmin.restful.shop.dao.ProductRepository;
import ro.hume.cosmin.restful.shop.dao.ShopOrderRepository;
import ro.hume.cosmin.restful.shop.model.FixedPriceProduct;
import ro.hume.cosmin.restful.shop.model.Product;
import ro.hume.cosmin.restful.shop.model.ShopOrder;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ShopOrderRepositoryIT {

	private static final long PRODUCT_ID = 1L;
	private static final String PRODUCT_NAME = "test";
	private static final BigDecimal PRODUCT_PRICE = BigDecimal.valueOf(0.99);
	private static final long ORDER_ID = 1L;
	private static final String CUSTOMER_EMAIL = "someone@example.com";
	private static final String UUID = "12345678-abcd-0000-9999-0123456789ab";
	private static final LocalDateTime ORDER_TIME = LocalDateTime.of(2018, 12, 10, 12, 34, 56);

	@Autowired
	private ProductRepository productDao;

	@Autowired
	private ShopOrderRepository shopOrderDao;

	@Test
	public void testSaveCreate() {
		Product product = new Product();
		product.setId(PRODUCT_ID);
		product.setName(PRODUCT_NAME);
		product.setPrice(PRODUCT_PRICE);
		product = productDao.save(product);

		FixedPriceProduct fixedPriceProduct = new FixedPriceProduct();
		fixedPriceProduct.setId(PRODUCT_ID);
		fixedPriceProduct.setProduct(product);
		fixedPriceProduct.setPrice(PRODUCT_PRICE);

		ShopOrder shopOrder = new ShopOrder();
		shopOrder.setId(ORDER_ID);
		shopOrder.setEmail(CUSTOMER_EMAIL);
		shopOrder.setUuid(UUID);
		shopOrder.setOrderTime(ORDER_TIME);
		shopOrder.setProducts(Arrays.asList(fixedPriceProduct));
		fixedPriceProduct.setShopOrder(shopOrder);

		ShopOrder savedOrder = shopOrderDao.save(shopOrder);

		assertNotNull(savedOrder.getId());
		assertNotEquals(Long.valueOf(0), savedOrder.getId());
		assertEquals(CUSTOMER_EMAIL, savedOrder.getEmail());
		assertEquals(UUID, savedOrder.getUuid());
		assertEquals(ORDER_TIME, savedOrder.getOrderTime());
	}

	@Test
	public void testRetrieve() {
		ShopOrder order2018March = saveShopOrder(2018, 03);
		ShopOrder order2018May = saveShopOrder(2018, 05);
		ShopOrder order2018July = saveShopOrder(2018, 07);

		LocalDateTime april = LocalDateTime.of(2018, 04, 01, 00, 00, 00);
		LocalDateTime june = LocalDateTime.of(2018, 06, 30, 23, 59, 59);

		List<ShopOrder> ordersBetweenAprilAndJune = shopOrderDao.find(april, june);

		assertEquals(1, ordersBetweenAprilAndJune.size());
		assertThat(ordersBetweenAprilAndJune, contains(order2018May));
		assertThat(ordersBetweenAprilAndJune, not(contains(order2018March)));
		assertThat(ordersBetweenAprilAndJune, not(contains(order2018July)));
		assertNotNull(ordersBetweenAprilAndJune.get(0).getProducts());
	}

	private ShopOrder saveShopOrder(int year, int month) {
		Product product = new Product();
		product.setId(PRODUCT_ID);
		product.setName(PRODUCT_NAME);
		product.setPrice(PRODUCT_PRICE);
		product = productDao.save(product);

		FixedPriceProduct fixedPriceProduct = new FixedPriceProduct();
		fixedPriceProduct.setProduct(product);
		fixedPriceProduct.setPrice(product.getPrice());

		LocalDateTime time = LocalDateTime.of(year, month, 01, 12, 34, 56);

		ShopOrder shopOrder = new ShopOrder();
		shopOrder.setEmail(CUSTOMER_EMAIL);
		shopOrder.setUuid(time.toString());
		shopOrder.setOrderTime(time);
		shopOrder.setProducts(Collections.singletonList(fixedPriceProduct));
		return shopOrderDao.save(shopOrder);
	}
}

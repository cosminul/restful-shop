package ro.hume.cosmin.restful.shop.api.data;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.modelmapper.ModelMapper;

import ro.hume.cosmin.restful.shop.model.Product;

public class ProductDataTest {

	private static final long PRODUCT_ID = 1L;
	private static final String PRODUCT_NAME = "test";
	private static final BigDecimal PRODUCT_PRICE = BigDecimal.valueOf(0.99);

	private ModelMapper modelMapper = new ModelMapper();

	@Test
	public void testConvertModelToData() {
		Product model = new Product();
		model.setId(PRODUCT_ID);
		model.setName(PRODUCT_NAME);
		model.setPrice(PRODUCT_PRICE);

		ProductData data = modelMapper.map(model, ProductData.class);

		assertEquals(Long.valueOf(PRODUCT_ID), data.getId());
		assertEquals(PRODUCT_NAME, data.getName());
		assertEquals(PRODUCT_PRICE, data.getPrice());
	}

	@Test
	public void testConvertDataToModel() {
		ProductData data = new ProductData();
		data.setId(PRODUCT_ID);
		data.setName(PRODUCT_NAME);
		data.setPrice(PRODUCT_PRICE);

		Product model = modelMapper.map(data, Product.class);

		assertEquals(Long.valueOf(PRODUCT_ID), model.getId());
		assertEquals(PRODUCT_NAME, model.getName());
		assertEquals(PRODUCT_PRICE, model.getPrice());
	}
}
